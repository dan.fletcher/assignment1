package main

import (
	"testing"
	"fmt"
)

func TestInit(t *testing.T){

	var active queue
	active.Init()
}

// A sign in / sign out test
func Test1SignIn(t *testing.T) {
		
	var active queue
	active.Init()


	id1 := create()
	
	active.AddUser(1, []int{2,3,4}, id1)

	if id1.Get() != "" || id1.Stat() != false {
	 	t.Errorf("output: %s :: status %v",  id1.Last(),  id1.Stat())
	}

	active.RemoveUser(1)

	if id1.Get() != "" || id1.Stat() != false {
		t.Errorf("output: %s :: status %v",  id1.Last(),  id1.Stat())
	}
	
}


// A 2 user sign in / out test
func Test2SignIn(t *testing.T) {
		
	var active queue
	active.Init()


	id1 := create()
	id2 := create()

	
	// 1st User signs in
	active.AddUser(1, []int{2,3,4}, id1)

	// 2nd User signs in
	active.AddUser(2, []int{1,3,4}, id2)

	if  id2.Get() != `{"user_id": 1, "online": true}` ||  id2.Stat() != false {
		t.Errorf("output: %s :: status %v",  id2.Last(),  id2.Stat())
	}

	if  id1.Get() != `{"user_id": 2, "online": true}` ||  id1.Stat() != false {
		t.Errorf("output: %s :: status %v",  id1.Last(),  id1.Stat())
	}

	// 1st Users leaves
	active.RemoveUser(1)

	if  id1.Get() != `` ||  id1.Stat() != false {
		t.Errorf("output: %s :: status %v",  id1.Last(),  id1.Stat())
	}
	
	if  id2.Get() != `{"user_id": 1, "online": false}` ||  id2.Stat() != false {
		t.Errorf("output: %s :: status %v",  id2.Last(),  id2.Stat())
	}

	// 2nd User leaves
	active.RemoveUser(2)
	
	if  id2.Get() != `` ||  id2.Stat() != false {
		t.Errorf("output: %s :: status %v",  id2.Last(),  id2.Stat())
	}
}

// A 3 user sig in / sign out
func Test3SignInOut(t *testing.T) {
		
	var active queue
	active.Init()


	id1 := create()
	id2 := create()
	id3 := create()
	
	// 1st User signs in
	active.AddUser(1, []int{2,3,4}, id1)

	// 2nd User signs in
	active.AddUser(2, []int{1,3,4}, id2)

	// 3nd User signs in
	active.AddUser(3, []int{1,2,4}, id3)

	if  id3.Get() != `{"user_id": 1, "online": true}{"user_id": 2, "online": true}` ||  id3.Stat() != false {
		t.Errorf("output: %s :: status %v",  id3.Last(),  id3.Stat())
	}

	if  id2.Get() != `{"user_id": 1, "online": true}{"user_id": 3, "online": true}` ||  id2.Stat() != false {
		t.Errorf("output: %s :: status %v",  id2.Last(),  id2.Stat())
	}

	if  id1.Get() != `{"user_id": 2, "online": true}{"user_id": 3, "online": true}` ||  id1.Stat() != false {
		t.Errorf("output: %s :: status %v",  id1.Last(),  id1.Stat())
	}

	// 1st Users leaves
	active.RemoveUser(1)

	if  id1.Get() != `` ||  id1.Stat() != false {
		t.Errorf("output: %s not %s :: status %v not %v ",  id1.Last(), "",  id1.Stat(), false)
	}
	
	if  id2.Get() != `{"user_id": 1, "online": false}` ||  id2.Stat() != false {
		t.Errorf("output: %s not %s :: status %v not %v ",  id2.Last(), "",  id2.Stat(), false)
	}

	if  id3.Get() != `{"user_id": 1, "online": false}` ||  id3.Stat() != false {
		t.Errorf("output: %s not %s :: status %v not %v ",  id3.Last(), "",  id3.Stat(), false)
	}

	// 2nd User leaves
	active.RemoveUser(2)
	
	if  id1.Get() != `` ||  id1.Stat() != false {
		t.Errorf("output: %s not %s :: status %v not %v ",  id1.Last(), "",  id1.Stat(), false)
	}
	
	if  id2.Get() != `` ||  id2.Stat() != false {
		t.Errorf("output: %s not %s :: status %v not %v ",  id2.Last(), "",  id2.Stat(), false)
	}

	if  id3.Get() != `{"user_id": 2, "online": false}` ||  id3.Stat() != false {
		t.Errorf("output: %s not %s :: status %v not %v ",  id3.Last(), "",  id3.Stat(), false)
	}
}

// A sign in / re-sign in test
func Test1ReSignIn(t *testing.T) {
		
	var active queue
	active.Init()


	id1 := create()
	id2 := create()
	
	// first sign in
	active.AddUser(1, []int{2,3,4}, id1)

	// re-sign in
	active.AddUser(1, []int{2,3,4}, id2)

	if  id1.Get() != "" ||  id1.Stat() != true {
	 	t.Errorf("output: %s :: status %v",  id1.Last(),  id1.Stat())
	}
	
	if  id2.Get() != "" ||  id2.Stat() != false {
	 	t.Errorf("output: %s :: status %v",  id2.Last(),  id2.Stat())
	}

	active.RemoveUser(1)

	if  id1.Get() != "" ||  id1.Stat() != true {
	 	t.Errorf("output: %s :: status %v",  id1.Last(),  id1.Stat())
	}

	if  id2.Get() != "" ||  id2.Stat() != false {
		t.Errorf("output: %s :: status %v",  id2.Last(),  id2.Stat())
	}
	
}

// A 3 user select friends test
func Test3Friends(t *testing.T) {
		
	var active queue
	active.Init()


	id1 := create()
	id2 := create()
	id3 := create()
	
	// 1st User signs in
	active.AddUser(1, []int{2}, id1)

	// 2nd User signs in
	active.AddUser(2, []int{1}, id2)

	// 3nd User signs in
	active.AddUser(3, []int{1,2}, id3)

		if  id1.Get() != `{"user_id": 2, "online": true}` ||  id1.Stat() != false {
		t.Errorf("output: %s :: status %v",  id1.Last(),  id1.Stat())
	}
	
		if  id2.Get() != `{"user_id": 1, "online": true}` ||  id2.Stat() != false {
		t.Errorf("output: %s :: status %v",  id2.Last(),  id2.Stat())
	}

	if  id3.Get() != `{"user_id": 1, "online": true}{"user_id": 2, "online": true}` ||  id3.Stat() != false {
		t.Errorf("output: %s :: status %v",  id3.Last(),  id3.Stat())
	}

	// 1st Users leaves
	active.RemoveUser(1)

	// 2ns Users leaves
	active.RemoveUser(2)

	// 3rd Users leaves
	active.RemoveUser(3)

	if  id1.Get() != `` ||  id1.Stat() != false {
		t.Errorf("output: %s not %s :: status %v not %v ",  id1.Last(), "",  id1.Stat(), false)
	}
	
	if  id2.Get() != `{"user_id": 1, "online": false}` ||  id2.Stat() != false {
		t.Errorf("output: %s not %s :: status %v not %v ",  id2.Last(), "",  id2.Stat(), false)
	}

	if  id3.Get() != `{"user_id": 1, "online": false}{"user_id": 2, "online": false}` ||  id3.Stat() != false {
		t.Errorf("output: %s not %s :: status %v not %v ",  id3.Last(), "",  id3.Stat(), false)
	}
}

// A friend myself test
func Test1FriendMyself(t *testing.T) {
		
	var active queue
	active.Init()


	id1 := create()
	
	active.AddUser(1, []int{1,2,3,4}, id1)

	if id1.Get() != "" || id1.Stat() != false {
	 	t.Errorf("output: %s :: status %v",  id1.Last(),  id1.Stat())
	}

	active.RemoveUser(1)

	if id1.Get() != "" || id1.Stat() != false {
		t.Errorf("output: %s :: status %v",  id1.Last(),  id1.Stat())
	}
	
}

type myRWC struct {
	received *[]byte
	last *[]byte
	status *bool
}

func create() myRWC {
	var my myRWC
	rec := []byte{}
	lst := []byte{}
	sta := false
	my.received = &rec
	my.last = &lst
	my.status = &sta
	return my
}

func (b myRWC) Get() string {
	str := string(*b.received)
	*b.last = *b.received
	*b.received = []byte{}
	return str
}

func (b myRWC) Last() string {
	return string(*b.last)
}

func (b myRWC) Stat() bool {
	return *b.status
}

func (b myRWC) Read([]byte) (int, error) {
	return 0, nil
}

func (b myRWC) Write(d []byte) (int, error) {
	*b.received = append(*b.received, d...)
	return len(d), nil
}

func (b myRWC) Close() error {
	if *b.status == false {
		*b.status = true
	} else {
		return fmt.Errorf("Already closed")
	}
	return nil
}

