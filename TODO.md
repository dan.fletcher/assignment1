release/0.1

- add panic control 
- on terminate don't close an already closed session
- error handling preferable a return and or loglevel
- command line options