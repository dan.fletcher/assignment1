package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
)

var connections []net.Conn

func main() {
	conn, err := net.Dial("tcp", "localhost:21")
	if err != nil {
		log.Fatalln(err)
	}
	defer conn.Close()

	receiveText := bufio.NewScanner(conn)
	sendText := bufio.NewReader(os.Stdin)

	// receiving data
	go func() {
		for {
			receiveText.Scan()
			ln := receiveText.Text()
			fmt.Println(ln)
		}
	}()

	// sending data
	for {
		ln, _ := sendText.ReadString('\n')
		_, err := io.WriteString(conn, ln)
		if err != nil {
			fmt.Println(err)
		}
	}
}
