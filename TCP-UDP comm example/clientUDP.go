package main

import (
	"bufio"
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/hex"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"time"
)

var connections []net.Conn

func main() {

	key, _ := hex.DecodeString("6368616e676520746869732070617373")

	conn, err := net.Dial("udp", ":21")

	if err != nil {
		log.Fatalln(err)
	}
	defer conn.Close()

	//keep alive
	go func() {
		for {
			io.WriteString(conn, "")
			time.Sleep(time.Second * 30)
		}
	}()

	buffer := make([]byte, 1000)
	sendText := bufio.NewReader(os.Stdin)

	// receiving data
	go func() {
		for {
			n, _ := conn.Read(buffer)
			inFile := bytes.NewReader(buffer[:n])

			block, err := aes.NewCipher(key)
			if err != nil {
				panic(err)
			}

			// If the key is unique for each ciphertext, then it's ok to use a zero
			// IV.
			var iv [aes.BlockSize]byte
			stream := cipher.NewOFB(block, iv[:])

			outFile := new(bytes.Buffer)

			reader := &cipher.StreamReader{S: stream, R: inFile}
			// Copy the input file to the output file, decrypting as we go.
			if _, err := io.Copy(outFile, reader); err != nil {
				panic(err)
			}
			fmt.Println(outFile.String())
		}
	}()

	// sending data
	for {
		ln, _ := sendText.ReadBytes('\n')

		inFile := bytes.NewReader(ln[:len(ln)-2])

		block, err := aes.NewCipher(key)
		if err != nil {
			panic(err)
		}

		// If the key is unique for each ciphertext, then it's ok to use a zero
		// IV.
		var iv [aes.BlockSize]byte
		stream := cipher.NewOFB(block, iv[:])

		writer := &cipher.StreamWriter{S: stream, W: conn}
		// Copy the input file to the output file, encrypting as we go.
		if _, err := io.Copy(writer, inFile); err != nil {
			panic(err)
		}
	}
}
