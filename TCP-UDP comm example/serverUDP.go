package main

import (
	"fmt"
	"log"
	"net"
)

var connections []net.Addr

func main() {
	li, err := net.ListenPacket("udp", ":21")
	if err != nil {
		log.Fatalln(err)
	}
	defer li.Close()

	buffer := make([]byte, 1000)
	for {
		n, addr, _ := li.ReadFrom(buffer)

		// This is keepalive handling
		if n == 0 {
			myself := false
			for _, a := range connections {
				if a.String() == addr.String() {
					myself = true
					break
				}
			}
			if !myself {
				connections = append(connections, addr)
				fmt.Println("added", connections)
			}
			continue
		}

		fmt.Println(n, addr, err, "::", string(buffer[:n]))

		myself := false
		for _, a := range connections {
			if a.String() == addr.String() {
				myself = true
			} else {
				b, err := li.WriteTo(buffer[:n], a)
				if err != nil {
					fmt.Println(b, err)
				}
			}
		}
		if !myself {
			connections = append(connections, addr)
			fmt.Println("added", connections)
		}
	}
}
