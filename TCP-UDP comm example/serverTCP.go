package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
)

var connections []net.Conn

func main() {
	li, err := net.Listen("tcp", ":21")
	if err != nil {
		log.Fatalln(err)
	}
	defer li.Close()

	for {
		conn, err := li.Accept()
		if err != nil {
			log.Println(err)
			continue
		}
		go handle(conn)
	}
}

func handle(conn net.Conn) {

	scanner := bufio.NewScanner(conn)

	connections = append(connections, conn)

	for scanner.Scan() {
		ln := scanner.Text()
		fmt.Println(conn.RemoteAddr(), ln)
		for _, c := range connections {
			if c != conn {
				io.WriteString(c, ln+"\r\n")
			}
		}
	}
	remove(conn)
	defer conn.Close()

	// we never get here
	// we have an open stream connection
	// how does the above reader know when it's done?
	fmt.Println("Code got here.")
}

func remove(c net.Conn) {

	var newConnections []net.Conn
	for _, c := range connections {
		newConnections = append(newConnections, c)
	}
	connections = newConnections
}
