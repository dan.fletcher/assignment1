Phases

1. Register
    You need a login page
    Login can be authenticated with the Database as login doesn't happen often
        Database can have many replicas, change password won't happen too often, so a single master with many replicas would be fine
        Databases can be subdivided into domains or something if needed
        Databases need pointers to the users inforamtion, this can be anything, you could use an ldap system or something. 
    Register your connection information. 
        Would be nice if you can contact a client directly but it seems unlikely, so we may need to use a websocket channel through the connected client
        This would mean communications need to flow direct from system 1 to system 2 to relay messages. 
        We need a system of lookup on the big scale. 

    Division
        Use a hash key to work out which systems look after which users. Maybe LDAP something designed for this purpose. 

    OK simple for us, we expose these functions so they can be modified later. 

2. Communicate
    We are just forwarding messages through websockets for now
    Consider a situation where we have multiple docker services, 


3. Docker Swarm
    - You have management nodes that don't do anything
    - You have worker nodes that relay the messages
    - You have worker nodes that act simply as proxy's, to forward the incomming request to an available node. I can probably find other socket layer proxies if needed.

4. Screen Pop
    - Need to provide the callers/called/agents number to the client to run a screen pop, so these 3 are values you can pass as a url or post

5. Status notifications
    - Kafka will be required for this I would expect, unsure.