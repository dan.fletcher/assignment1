module Training_examples/ComApp/cmd/register

go 1.13

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	pkg_config v0.0.0-00010101000000-000000000000
	pkg_srvmgr v0.0.0-00010101000000-000000000000
)

replace (
	pkg_config => ./vendor/pkg_config
	pkg_srvmgr => ./vendor/pkg_srvmgr
)
