/*
	Just reliable service manager here

	We need to listen on a websocket
	Pass the registration request to the registration package

*/

package main

import (
	configfile "inf-stash-01.gammatelecom.com/scm/df/pkg_config"
	srv "inf-stash-01.gammatelecom.com/scm/df/pkg_srvmgr"
	"net/http"
	"net"
	"fmt"
	"github.com/gorilla/websocket"
	"time"
	"context"
	"errors"
	"log"
)

// GitCommit value is created at buildtime see dockerfile
var GitCommit string

// GitBranch value is created at buildtime see dockerfile
var GitBranch string

// GitRepo value is created at buildtime see dockerfile
var GitRepo string

var helpAuthors = "Dan Fletcher <daniel.fletcher@gamma.co.uk>"
var helpMsg = `
This is the client registration / msgforwarder
It will listen/establish and maintain a websocket connection to the client
It will accept authentication over the websocket
It will then register the information needed to contact the client.
`

var clients = make(map[string]*websocket.Conn)
var registrationServer net.Conn

func main() {
	fmt.Println("Starting...")

	// Starting the registration server connection
	var err error
	registrationServer, err = net.Dial("tcp", ":9000")
	if err != nil {
		log.Fatal("Unable to establish connection to registraion Server", err)
	}
	// End registration server bit


	var (
		localConf map[string]string
	)

	//
	// Initilazion stuff
	//

	// Pull in the config file or command line options
	localConf = configfile.Load()

	// Display the help message
	if _, ok := localConf["help"]; ok {
		fmt.Printf(`
GitRepo:   %v
GitBranch: %v
GitCommit: %v
Author:    %v

%v
	`, GitRepo, GitBranch, GitCommit, helpAuthors, helpMsg)
	}


	// // Connect to the database
	// fmt.Println("Trying DB...")
	// err := mydb.Connect(localConf["dbUser"], localConf["dbPass"], localConf["dbName"], localConf["dbHost"])
	// if err != nil {
	// 	fmt.Println("I have a problem connecting to the database, not much point going any further")
	// 	fmt.Println(err)
	// 	os.Exit(1)
	// }
	// defer mydb.Close()

	// // import setting from the db
	// import_config()

	// // Open the two log files
	// fmt.Println("Opening Logs...")
	// logfile.Open(conf["stdLog"], conf["stdError"])
	// if err != nil {
	// 	fmt.Println("I have a problem opening log files, but will continue")
	// 	fmt.Println(err)
	// }
	// defer logfile.Close()

	service, _ := srv.New(srv.Timeout(time.Second * 1))
	defer service.TerminationHandling()

	// running a continious loop
	service.RunFunction("httpServer function", func(ctx context.Context) {
	loop:
		for {
			select {
			case <-ctx.Done():
				break loop
			default:
				// Create a simple file server
				fs := http.FileServer(http.Dir("public"))
				http.Handle("/", fs)

				// Configure websocket route
				http.HandleFunc("/ws", newConnections)
				err := http.ListenAndServe(":8000", nil)
				if err != nil {
					fmt.Println("ListenAndServe Error: ", err)
				}
			}
		}
	})

	service.RunFunction("forwarding function", func(ctx context.Context) {
	loop:
		for {
			select {
			case <-ctx.Done():
				break loop
			default:
				sock, err := net.Listen("tcp", ":9001")
				if err != nil {
					log.Fatal("Unable to listen on socket 9001. ", err)
				}
				defer sock.Close()

				for {
					conn, err := sock.Accept()
					if err != nil {
						fmt.Println("Failed to accept connection. ", err)
					}
					fmt.Println("accepted Connection")

					go func(conn net.Conn) {
						var data = json.NewDecoder(conn)
						var msg WebsocketMessage
						var lookup *websocket.Conn
						var ok bool

						for {

							err := data.Decode(msg)
							if err != nil {
								fmt.Println("Error reading json message from socket", conn.RemoteAddr(), err)
								break
							}
					
							lookup, ok = clients[msg.Recepient]
							if != ok {
								i, err = msgServer.Write(json.Marshal(WebsocketMessage{
									Event: "err",
									Data: "User not found"
								})))
								if err != nil {
									fmt.Println("Unable to send message to forwarding Server", err)
									return errors.New("Internal Error")
								}
							}
						}
						conn.Close()
					}(conn)
				}
		}
	})
}

func newConnections(w http.ResponseWriter, r *http.Request) {
	var msg WebsocketMessage
	var username string
	var upgrader = websocket.Upgrader{}
	var err error
	var ok bool

	// Upgrade initial GET request to a websocket
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("WS-Upgrade: failed to establish websocket:", err)
		return 
	}
	defer ws.Close()

	for {		
		
	// Read and decode message
		err = ws.ReadJSON(&msg)	
		if err != nil {
			// err = ws.WriteJSON(WebsocketMessage{
			// 	Event: "err", 
			// 	Data: "Did not understand message",
			// })
			break
		}
		fmt.Println("Received message:", msg)

	// Register event
		if msg.Event == "register" {
			if ok, err = register(msg.Param1, msg.Param2, ws.RemoteAddr(), ws); !ok {
				if err != nil {
					err = ws.WriteJSON(WebsocketMessage{
						Event: "err", 
						Data: "System error",
					})
				} else {
					err = ws.WriteJSON(WebsocketMessage{
						Event: "err", 
						Data: "Registration failed",
					})
				}	
				if err != nil {
					fmt.Println("Register-Failed: sending reply failed:", err)
				}
				continue
			}
				
			username = msg.Param1
			err = ws.WriteJSON(WebsocketMessage{
				Event: "ok", 
				Data: "Registration successful",
			})
			if err != nil {
				fmt.Println("Register-Success: sending reply failed:", err)
			}
			continue
		}

	// Stop executing if not logged in
		if username == "" {
			err = ws.WriteJSON(WebsocketMessage{
				Event: "err", 
				Data: "Not logged in",
			})
			if err != nil {
				fmt.Println("Not-Authenticated: Sending reply failed:", err)
			}
			continue
		}

	// Unregister event
		if msg.Event == "unregister" {
			err = unregister(username, ws.RemoteAddr())
			if err != nil {
				ws.WriteJSON(WebsocketMessage{
					Event: "err",
					Data: "Unregistration failed",
				})
			} else {
				ws.WriteJSON(WebsocketMessage{
					Event: "ok", 
					Data: "Unregistration sucessful",
				})
			}
			break
		}

	// Forward event
		if msg.Event == "forward" {
			msg.Sender = username
			err = forwarder(msg)
			if err != nil {
				err = ws.WriteJSON(WebsocketMessage{
					Event: "err", 
					Data: fmt.Sprint(err),
				})
			} else {
				err = ws.WriteJSON(WebsocketMessage{
					Event: "ok",
					Data: "forwarded sucessfully",
				})
			}
			if err != nil {
				fmt.Println("forwarding: Sending reply failed:", err)
			}
			continue
		}
	}
	
	// Close and cleanup
	if username != "" {
		delete(clients, username)
		fmt.Println("Cleanup-Unregister: session closed before logout for user:", username)
	}
	ws.Close()
	fmt.Println("Closing Session")
}

func register(username string, password string, ip net.Addr, ws *websocket.Conn) (bool, error) {
	var data = make([]byte, 100)
	var i int

	if password != "password" {
		return false, nil
	}
	
	// Register with external server
	registrationServer.Write([]byte("register:"+username))
	i, _ = registrationServer.Read(data)
	data = data[:i]
	fmt.Println("REGISTRATION:", string(data))

	// Terminate existing session
	if lookup, ok := clients[username]; ok {
		lookup.Close()
	}

	clients[username] = ws
	fmt.Println("registering:", username)
	
	return true, nil
}

func unregister(username string, ip net.Addr) error {
	var data = make([]byte, 100)
	var i int

	// UnRegister with external server
	registrationServer.Write([]byte("unregister:"+username))
	i, _ = registrationServer.Read(data)
	data = data[:i]
	fmt.Println("UNREGISTRATION:", string(data))


	// Remove an identifier from the channel
	delete(clients, username)
	// unregister on master database
	fmt.Println("unregistering:", username)
	return nil
}

func forwarder(msg WebsocketMessage) error {
	var err error
	var ok bool
	var lookup *websocket.Conn
	var arr []string
	var i int
	
	//lookup, ok = clients[msg.Param1]

	// lookup from external lookup server
	i, err = registrationServer.Write([]byte("lookup:"+msg.Recepient))
		if err != nil {
		fmt.Println("Error sending message to lookup server:", err)
		return errors.New("Internal Error")
	}

	// Read replay from lookup server
	i, err = registrationServer.Read(data)
	if err != nil {
		fmt.Println("Error reading reply from lookup server:", err)
		return errors.New("Internal Error")
	}
	data = data[:i]
	arr = strings.Split(string(data),":")
	fmt.Println("LOOKUP:", string(data))

	// Return error if not found
	if arr[0] == "err" {
		fmt.Println("User was not found during lookup")
		return errors.New("Recepient lookup failed")
	}

	// Contact forwarding server
	msgServer, err = net.Dial("tcp", arr[1]":9001")
	if err != nil {
		fmt.Println("Unable to establish connection to forwarding Server", err)
		return errors.New("Internal Error")
	}
	defer msgServer.Close()

	// Forward message
	i, err = msgServer.Write(json.Marshal(msg)))
	if err != nil {
		fmt.Println("Unable to send message to forwarding Server", err)
		return errors.New("Internal Error")
	}

	// Read forwarding servers reply
	i, err = msgServer.Read(data)
	if err != nil {
		fmt.Println("Unable to receive message from forwarding Server", err)
		return errors.New("Internal Error")
	}data = data[:i]
	arr = strings.Split(string(data),":")

	// Return error if not found
	if arr[0] == "err" {
		fmt.Println("Forwarding server did not forward the message:", err)
		return errors.New("Forward failed")
	}

	return nil
}

// Define our message object
type WebsocketMessage struct {
	Event  string `json:"event"`
	Sender string `json:"sender"`
	Recepient string `json:"recepient"`
	ID int `json:"id"`
	Data   string `json:"data"`
	Param1 string `json:"param1"`
	Param2 string `json:"param2"`
	Param3 string `json:"param3"`
	Param4 string `json:"param4"`
	Param5 string `json:"param5"`
	Param6 string `json:"param6"`
}