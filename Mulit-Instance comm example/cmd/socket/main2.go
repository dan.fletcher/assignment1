/*
	Just reliable service manager here

	We need to listen on a websocket
	Pass the registration request to the registration package

*/

package main

import (
//	configfile "pkg_config"
	srv "pkg_srvmgr"
	"net/http"
	"net"
	"fmt"
	"github.com/gorilla/websocket"
	"time"
	"context"
	"errors"
	"log"
	"encoding/json"
	"strings"
	"os"
)

// GitCommit value is created at buildtime see dockerfile
var GitCommit string

// GitBranch value is created at buildtime see dockerfile
var GitBranch string

// GitRepo value is created at buildtime see dockerfile
var GitRepo string

var helpAuthors = "Dan Fletcher <daniel.fletcher@gamma.co.uk>"
var helpMsg = `
This is the client registration / msgforwarder
It will listen/establish and maintain a websocket connection to the client
It will accept authentication over the websocket
It will then register the information needed to contact the client.
`

var clients = make(map[string]*websocket.Conn)
var registrationServer net.Conn
var regChan = make(chan registrationRequests, 1000)
var hostname string

func main() {
	fmt.Println("Starting...")

	hostname, _ = os.Hostname()

	// Starting the registration server connection
	var err error
	registrationServer, err = net.Dial("tcp", "lookup_a:9000")
	if err != nil {
		log.Fatal("Unable to establish connection to registraion Server", err)
	}
	// End registration server bit



	service, _ := srv.New(srv.Timeout(time.Second * 1))
	defer service.TerminationHandling()

	// running a continious loop
	service.RunFunction("httpServer service", func(ctx context.Context) {
	loop:
		for {
			select {
			case <-ctx.Done():
				break loop
			default:
				// Create a simple file server
				fs := http.FileServer(http.Dir("public"))
				http.Handle("/", fs)

				// Configure websocket route
				http.HandleFunc("/ws", newConnections)
				err := http.ListenAndServe(":8000", nil)
				if err != nil {
					fmt.Println("ListenAndServe Error: ", err)
				}
			}
		}
	})

	service.RunFunction("routing service", func(ctx context.Context) {
	loop:
		for {
			var err error 
			var i int
			buffer := make([]byte, 1000)
			var msg WebsocketMessage
			var addr net.Addr

			// Open socket to receive messages
			sock, err := net.ListenPacket("udp", ":8001")
			if err != nil {
				log.Fatal("Unable to listen on socket 8001. ", err)
			}
			defer sock.Close()

			select {
			case <-ctx.Done():
				break loop
			default:
				for {
					i, addr, err = sock.ReadFrom(buffer)

					if err != nil {
						fmt.Println("Error reading socket", addr, err)
						continue
					}

					err = json.Unmarshal(buffer[:i], &msg)
					if err != nil {
						fmt.Println("Error decoding message", addr, err)
						continue
					}

					// Forward event
					if msg.Event == "forward" {
						lookup, ok := clients[msg.Recepient]
						if !ok {
							fmt.Println("Received a forward from:", msg.Sender, "for:", msg.Recepient, "but I dont know them")
						} else {
							err = lookup.WriteJSON(msg)
							if err != nil {
								fmt.Println("Forwarding message from:", msg.Sender, "for:", msg.Recepient, "Errored:", err)
							}
						}
						continue
					}
					
					if msg.Event == "unregister" {
						unregister(msg.Recepient)

						lookup, ok := clients[msg.Recepient]
						if !ok {
							fmt.Println("Received a unregister for:", msg.Recepient, "but I dont know them")
						} else {
							lookup.Close()
						}
						continue
					}
				}
			}
			
			// // Open socket to receive messages
			// sock, err := net.Listen("tcp", ":8001")
			// if err != nil {
			// 	log.Fatal("Unable to listen on socket 8001. ", err)
			// }
			// defer sock.Close()

			// select {
			// case <-ctx.Done():
			// 	break loop
			// default:
			// 	for {
			// 		conn, err := sock.Accept()
			// 		if err != nil {
			// 			fmt.Println("Failed to accept connection. ", err)
			// 		}
			// 		fmt.Println("accepted Connection")

			// 		go func(conn net.Conn) {
			// 			var data = make([]byte, 10000)
			// 			var msg WebsocketMessage

			// 			for {
			// 				i, err := conn.Read(data)
			// 				if err != nil {
			// 					fmt.Println("Error reading socket", conn.RemoteAddr(), err)
			// 					break
			// 				}

			// 				err = json.Unmarshal(data[:i], &msg)
			// 				if err != nil {
			// 					fmt.Println("Error decoding message", conn.RemoteAddr(), err)
			// 					break
			// 				}

			// 				// Forward event
			// 				if msg.Event == "forward" {
			// 					lookup, ok := clients[msg.Recepient]
			// 					if !ok {
			// 						rmsg, _ := json.Marshal(WebsocketMessage{
			// 							Event: "err:user uncontactable", 
			// 							Data: fmt.Sprint(err),
			// 						})
			// 						_, err = conn.Write(rmsg)
			// 					} else {
			// 						err = lookup.WriteJSON(msg)
			// 						rmsg, _ := json.Marshal(WebsocketMessage{
			// 							Event: "ok",
			// 							Data: "forwarded sucessfully",
			// 						})
			// 						_, err = conn.Write(rmsg)
			// 					}
			// 					if err != nil {
			// 						fmt.Println("forwarding: Sending reply failed:", err)
			// 					}
			// 					continue
			// 				}
			// 			}
			// 			conn.Close()
			// 		}(conn)
			// 	}
			// }
		}
	})

	// service.RunFunction("lookup client", func(ctx context.Context) {
	// loop:
	// 	for {
	// 		conn, err := net.Dial("tcp", "lookup_a:9000")
	// 		if err != nil {
	// 			log.Fatal("Unable to establish connection to registraion Server", err)
	// 		}

	// 		var data registrationRequests
	// 		var msg []byte
	// 		select {
	// 		case <-ctx.Done():
	// 			break loop
	// 		case data = <-regChan:
	// 			msg, err = json.Marshal(data)
	// 			_, _ = conn.Write(msg)
	// 		}
	// 	}
	// })
}

func newConnections(w http.ResponseWriter, r *http.Request) {
	var msg WebsocketMessage
	var username string
	var upgrader = websocket.Upgrader{}
	var err error
	var ok bool

	// Upgrade initial GET request to a websocket
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("WS-Upgrade: failed to establish websocket:", err)
		return 
	}
	defer ws.Close()

	for {		
		
	// Read and decode message
		err = ws.ReadJSON(&msg)	
		if err != nil {
			// err = ws.WriteJSON(WebsocketMessage{
			// 	Event: "err", 
			// 	Data: "Did not understand message",
			// })
			break
		}
		fmt.Println("Received message:", msg)

	// Register event
		if msg.Event == "register" {
			if ok, err = register(msg.Param1, msg.Param2, ws.RemoteAddr(), ws); !ok {
				if err != nil {
					err = ws.WriteJSON(WebsocketMessage{
						Event: "err", 
						Data: "System error",
					})
				} else {
					err = ws.WriteJSON(WebsocketMessage{
						Event: "err", 
						Data: "Registration failed",
					})
				}	
				if err != nil {
					fmt.Println("Register-Failed: sending reply failed:", err)
				}
				continue
			}
				
			username = msg.Param1
			err = ws.WriteJSON(WebsocketMessage{
				Event: "ok", 
				Data: "Registration successful",
			})
			if err != nil {
				fmt.Println("Register-Success: sending reply failed:", err)
			}
			continue
		}

	// Stop executing if not logged in
		if username == "" {
			err = ws.WriteJSON(WebsocketMessage{
				Event: "err", 
				Data: "Not logged in",
			})
			if err != nil {
				fmt.Println("Not-Authenticated: Sending reply failed:", err)
			}
			continue
		}

	// Unregister event
		if msg.Event == "unregister" {
			err = unregister(username)
			if err != nil {
				ws.WriteJSON(WebsocketMessage{
					Event: "err",
					Data: "Unregistration failed",
				})
			} else {
				ws.WriteJSON(WebsocketMessage{
					Event: "ok", 
					Data: "Unregistration sucessful",
				})
			}
			break
		}

	// Forward event
		if msg.Event == "forward" {
			msg.Sender = username
			err = forwarder(msg)
			if err != nil {
				err = ws.WriteJSON(WebsocketMessage{
					Event: "err", 
					Data: fmt.Sprint(err),
				})
			}
			if err != nil {
				fmt.Println("forwarding: Sending reply failed:", err)
			}
			continue
		}
	}
	
	// Close and cleanup
	if username != "" {
		unregister(username)
		fmt.Println("Cleanup register: session closed before logout for user:", username)
	}
	ws.Close()
	fmt.Println("Closing Session")
}

func register(username string, password string, ip net.Addr, ws *websocket.Conn) (bool, error) {
	var data = make([]byte, 100)
	var i int
	var arr []string
	var err error
	var conn net.Conn

	if password != "password" {
		return false, nil
	}
	
	// Register with external server
	
	registrationServer.Write([]byte("register:"+hostname+":"+username))
	i, _ = registrationServer.Read(data)

	arr = strings.Split(string(data[:i]),":")
	if arr[0] == "reregistered" {

		if arr[2] != hostname {
			conn, err = net.Dial("udp", arr[2]+":8001")
			if err != nil {
				fmt.Println("Unable to establish connection to previous registered Server", err)
			}
			defer conn.Close()

			// Unregister old login
			data, err = json.Marshal(WebsocketMessage{
				Event: "unregister",
				Sender: username,
				Recepient: username,
			})

			_, err = conn.Write(data)
			if err != nil {
				fmt.Println("Unable to send message to forwarding Server", err)
			}
		} else {
			if lookup, ok := clients[username]; ok {
				lookup.Close()
			}
		}
	}
	
	fmt.Println("REGISTRATION:", string(data[:i]))

	// Terminate existing session
	if lookup, ok := clients[username]; ok {
		lookup.Close()
	}

	clients[username] = ws
	fmt.Println("registering:", username)
	
	return true, nil
}

func unregister(username string) error {
	var data = make([]byte, 100)
	var i int

	// UnRegister with external server
	registrationServer.Write([]byte("unregister:"+hostname+":"+username))
	i, _ = registrationServer.Read(data)
	data = data[:i]
	fmt.Println("UNREGISTRATION:", string(data))


	// Remove an identifier from the channel
	delete(clients, username)
	// unregister on master database
	fmt.Println("unregistering:", username)
	return nil
}

func forwarder(msg WebsocketMessage) error {
	var err error
	var arr []string
	var i int
	var data = make([]byte, 10000)
	var conn net.Conn
	
	//lookup, ok = clients[msg.Param1]

	// lookup from external lookup server
	i, err = registrationServer.Write([]byte("lookup:"+msg.Recepient))
		if err != nil {
		fmt.Println("Error sending message to lookup server:", err)
		return errors.New("Internal Error")
	}

	// Read replay from lookup server
	i, err = registrationServer.Read(data)
	if err != nil {
		fmt.Println("Error reading reply from lookup server:", err)
		return errors.New("Internal Error")
	}

	arr = strings.Split(string(data[:i]),":")
	fmt.Println("LOOKUP:", string(data[:i]))

	// Return error if not found
	if arr[0] == "err" {
		fmt.Println("User was not found during lookup")
		return errors.New("Recepient lookup failed")
	}

	// Contact forwarding server
	conn, err = net.Dial("udp", arr[1]+":8001")
	if err != nil {
		fmt.Println("Unable to establish connection to forwarding Server", err)
		return errors.New("Internal Error")
	}
	defer conn.Close()

	// Forward message
	data, err = json.Marshal(msg)

	i, err = conn.Write(data)
	if err != nil {
		fmt.Println("Unable to send message to forwarding Server", err)
		return errors.New("Internal Error")
	}

	return nil
}

// Registration Requests
type registrationRequests struct {
	socket *websocket.Conn
	request WebsocketMessage
}

// Define our message object
type WebsocketMessage struct {
	Event  string `json:"event"`
	Sender string `json:"sender"`
	Recepient string `json:"recepient"`
	ID int `json:"id"`
	Data   string `json:"data"`
	Param1 string `json:"param1"`
	Param2 string `json:"param2"`
	Param3 string `json:"param3"`
	Param4 string `json:"param4"`
	Param5 string `json:"param5"`
	Param6 string `json:"param6"`
}
