/*
	This is the user lookup system
	You can easily replace for something else its just for practice
*/


package main

import(
	"net"
	"fmt"
	"log"
	"strings"
)

var register = make(map[string]string)

func main() {
	fmt.Println("Lookup Running...")
	sock, err := net.Listen("tcp", ":9000")
	if err != nil {
		log.Fatal("Unable to listen on socket 9000. ", err)
	}
	defer sock.Close()

	for {
		conn, err := sock.Accept()
		if err != nil {
			fmt.Println("Failed to accept connection. ", err)
		}
		fmt.Println("accepted Connection")

		go func(conn net.Conn) {
			var msg = make([]byte, 100)
			var arr []string
			for {
				i, err := conn.Read(msg)
				if err != nil {
					fmt.Println("Error reading socket", conn.RemoteAddr(), err)
					break
				}
				arr = strings.Split(string(msg[:i]),":")
				//fmt.Println(string(msg[:i]))
				if arr[0] == "lookup" {
					lookup, ok := register[arr[1]]
					if !ok {
						conn.Write([]byte("err:not found"))
					} else {
						fmt.Println("Lookup reply user:", arr[1], "is at", lookup)
						conn.Write([]byte("found:"+lookup))
					}
					continue
				}
				if arr[0] == "register" {
					lookup, ok := register[arr[2]]
					if !ok {
						register[arr[2]] = arr[1]
						fmt.Println("New registration user:", arr[2], "on", arr[1])
						conn.Write([]byte("registered:"+arr[2]))
					} else {
						register[arr[2]] = arr[1]
						fmt.Println("Updated registration user:", arr[2], "to", arr[1])
						conn.Write([]byte("reregistered:"+arr[2]+":"+lookup))
					}
					continue
				}
				if arr[0] == "unregister" {
					delete(register, arr[2])
					fmt.Println("Unregister user:", arr[2], "from", arr[1])
					conn.Write([]byte("unregistered:"+arr[2]))
					continue
				}
				conn.Write([]byte("err:unknown request"))
			}
			conn.Close()
		}(conn)
	}
}

