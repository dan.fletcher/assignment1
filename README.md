Assignment1
===========



Credits
-------
Author:       Dan Fletcher
Date:         20/01/2021
Codebase:     https://gitlab.com/dan.fletcher/assignment1.git
Description:  TCP Service Assignment
Release:      0.1
Language:     golang


Assignment
----------

Write a TCP service in a language of your choice. If you are comfortable with Go, use that, but other languages are acceptable as long as you can explain the pros/cons and scaling characteristics.The service should have the following end points:

1. Start listening for tcp/udp connections.
2. Be able to accept connections.
3. Read json payload ({"user_id": 1, "friends": [2, 3, 4]})
{"user_id": 1, "friends": [2, 3, 4]}
{"user_id": 2, "friends": [1, 3, 4]}
4. After establishing successful connection - "store" it in memory the way you like.
5. When another connection established with the user_id from the list of any other user's "friends" section, they should be notified about it with message {"online": true}

( I am going to deviate on this slightly, it would seem sensible to include the name of the friend who is coming online or offline, and you equally would want a list of your friends when you join I will use the message {"user_id", "online": true})

6. When the user goes offline, his "friends" (if it has any and any of them online) should receive a message {"online": false}


Features
--------

