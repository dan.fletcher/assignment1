/*


*/

package main

import (
	"net"
	"fmt"
	"os"
	"encoding/json"
)

var active queue

func init() {
	active.Init()
}


func main() {
	fmt.Println("Starting...")

	// Start TCP Connection
	sock, err := net.Listen("tcp", ":21")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to listen on socket %v, error was %v", err, err)
		os.Exit(1)
	}
	defer sock.Close()

	for {
		conn, err := sock.Accept()
		if err != nil {
			fmt.Println("Failed to accept connection. ", err)
		}
		fmt.Println("accepted Connection from", conn.RemoteAddr())

		// break to a seporate function for tidyness
		go func(conn net.Conn) {
			msgJSON := json.NewDecoder(conn)
			msgStruct := new(Message)

			for {

				// Unmarshal message
				err := msgJSON.Decode(msgStruct)
				if err != nil {
					fmt.Println("Error reading message from", conn.RemoteAddr(), err)
					return
				}
				active.AddUser(msgStruct.UserId, msgStruct.Friends, conn)
			}
			active.RemoveUser(msgStruct.UserId)
			conn.Close()
		}(conn)
	}
}

// Define our message object
type Message struct {
	UserId  int `json:"user_id"`
	Friends []int `json:"friends,omitempty"`
}