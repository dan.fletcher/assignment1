package main

import (
	"io"
	"sync"
	"fmt"
	"strconv"
)

type queue struct {
	sync.RWMutex
	users map[int]user

	// use a reverse index to speed up notify messages, empty struct allocates no memory
	notify map[int]map[int]struct{}
}

type user struct {
	conn io.ReadWriteCloser
	friends []int // watch-list
}


// initilize maps
func (q *queue) Init() {
	if q.users == nil {
		q.Lock()
		defer q.Unlock()
		q.users = make(map[int]user)
		q.notify = make(map[int]map[int]struct{})
	}
}


// Add a new sign-in user
func (q *queue) AddUser(userid int, friends []int, conn io.ReadWriteCloser) {
	
	// Don't friend myself
	f := []int{}
	for _, friend := range friends {
		if friend == userid {
			continue
		}
		f =	append(f, friend)
	}
	friends = f

	// Register new user
	q.register(userid, friends, conn)		
	
	// Notify other users I am online
	q.notifyFriends(userid, true)
	
	// find my online friends
	q.notifyMe(userid, friends, conn)
}

// Add new user into q.users
func (q *queue) register(userid int, friends []int, conn io.ReadWriteCloser){

	// Terminate any existing connection
	if thisUser, ok := q.users[userid]; ok {
		// Kill the exiting user
		fmt.Printf("user %v already signed in, terminating existing connection/n", userid)
		err := thisUser.conn.Close()
		if err != nil {
			fmt.Printf("Error terminating connection for %v, error was %v/n", userid, err)
		}
		q.RemoveUser(userid)
	}

	// Add this connection request
	thisUser := user{}
	thisUser.conn = conn
	thisUser.friends = friends
	q.Lock()
	q.users[userid] = thisUser
	q.Unlock()

	// add reverse index
	q.addIndex(userid, friends)
}

// Cleanup after a session ends
func (q *queue) RemoveUser(userid int) {
	
	// Just check this user exists
	if thisUser, ok := q.users[userid]; ok {

		// Notify friends I am leaving
		q.notifyFriends(userid, false)

		// Remove me from the index
		q.removeIndex(userid, thisUser.friends)

		// Remove user
		q.Lock()
		delete(q.users, userid)
		q.Unlock()
	}	
}

// Notify users of my status
func (q *queue) notifyFriends(userid int, status bool) {

	notifyFriends, ok := q.notify[userid]

	if ok {
		for friend, _ := range notifyFriends {
			
			if thisUser, ok := q.users[friend]; ok {
				
				// its quite simple text we don't need json encoder
				_, err := thisUser.conn.Write([]byte(`{"user_id": `+strconv.Itoa(userid)+`, "online": `+strconv.FormatBool(status)+`}`))
				if err != nil {
					fmt.Printf("Error sending message to %v, error was %v/n", userid, err)
				}
			}
		}
	}
}

// Notify me of my friends online
func (q *queue) notifyMe(userid int, friends []int, conn io.ReadWriteCloser) {

	for _, friend := range friends {

		if _, ok := q.users[friend]; ok {
			
			// its quite simple text we don't need json encoder
			_, err := conn.Write([]byte(`{"user_id": `+strconv.Itoa(friend)+`, "online": true}`))
			if err != nil {
				fmt.Printf("Error sending message to %v, error was %v/n", userid, err)
			}
		}
	}

}

// Add the reverse index
// map[watch-list]map[notify-this-user]
func (q *queue) addIndex(userid int, friends []int) {
	
	// Add our user to watch index
	for _, friend := range friends {
		
		q.Lock()
		if _, ok := q.notify[friend]; !ok {
			q.notify[friend] = make(map[int]struct{})
		}

		// Add our user to watch this friend
		q.notify[friend][userid] = struct{}{}
		q.Unlock()
	}
}

// Remove user from reverse index
// map[watch-list]map[notify-this-user]
func (q *queue) removeIndex(userid int, friends []int) {

	// remove this users entries from notify index
	for _, friend := range friends {
		
		q.Lock()
		if lookup, ok := q.notify[friend]; ok {
			// remove our user from the watch index
			delete(lookup, friend)
		}		
		q.Unlock()
	}
}
				
				